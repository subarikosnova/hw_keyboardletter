const buttons = document.querySelectorAll(".btn");
let activeButton = null;

buttons.forEach(button => {
    button.addEventListener("click", () => {
        if (activeButton) {
            activeButton.classList.remove("active");
        }
        button.classList.add("active");
        activeButton = button;
    });
});